#!/bin/bash
# Script to synchronize all contacts in group and trust all new keys
account=$(awk -F "=" '/account/ {print $2}' ../config.ini)
sleep 3


echo "{\"type\": \"subscribe\", \"account\": \"${account}\", \"version\": \"v1\"}" | nc -U -w 10 /var/run/signald/signald.sock
sleep 1

echo "{\"type\": \"unsubscribe\", \"account\": \"${account}\", \"version\": \"v1\"}" | nc -U -w 2 /var/run/signald/signald.sock
sleep 3
signaldctl key trust-all
sleep 3

exit 0
