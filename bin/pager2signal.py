#!/usr/bin/env python
#coding: utf8

import time
import datetime
import RPi.GPIO as GPIO
import os
import subprocess
import logging
import logging.handlers
from configparser import ConfigParser
import socket

#Read config.ini file
config_object = ConfigParser()
path = os.path.join(os.path.dirname(os.path.dirname(__file__)))
config_object.read(os.path.join(path, 'config.ini'))
#Get the recepient
message = config_object["MESSAGE"]

logfile_dir = os.path.dirname(__file__)

def log_setup():
    logfilename = os.path.join(logfile_dir, '../log/pager2signal.log')
    log_handler = logging.handlers.WatchedFileHandler(logfilename)
    formatter = logging.Formatter(
        '%(asctime)s %(levelname)s pager2signal.py [%(process)d]: %(message)s',
        '%b %d %H:%M:%S')
    log_handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)

log_setup()


logging.info('============================================================')
logging.info('===================== SCRIPT INITIATED =====================')
logging.info('recepient: %s' ,format(message["recepient"]))
logging.info('text: %s' ,format(message["text"]))
logging.info('============================================================')


# Set counting method of the pins
GPIO.setmode(GPIO.BOARD)

# Set pin 18 (GPIO 24) as input
GPIO.setup(18, GPIO.IN, pull_up_down = GPIO.PUD_UP)

# counter
i = 0
a = 0

# function sending alert
def sendAlert():
    global sp
    sp = subprocess.Popen("signaldctl message send {}".format(message["recepient"])+" {}".format(message["text"]), shell=True, stdout=subprocess.PIPE)
    sub_out = "\n" + sp.stdout.read().decode('utf8')
    logging.info(sub_out)
    logging.info('alert sended')

## function sending alive
## To activate uncomment following lines and paste Signal ID of contact/group instead of [ENTER SIGNAL ID HERE] on line 66
## Also uncomment line 96-100
# def sendAlive():
#     global spalive
#     spalive = subprocess.Popen("signaldctl message send [ENTER SIGNAL ID HERE] alive", shell=True, stdout=subprocess.PIPE)
#     sub_out = "\n" + spalive.stdout.read().decode('utf8')
#     logging.info(sub_out)
#     logging.info('alive sended')


# function for input HIGH
def doIfHigh(channel):
    logging.info('Channel high detected')
    global a
    while (GPIO.input(18) == GPIO.LOW) and (a < 10):  # to prevent false alarm due to radio interference GPIO must be high for 1 second
        a = a + 1
        time.sleep(0.1)
        if a == 10:
            logging.info('Channel 10 times high detected')
            sendAlert()
#            time.sleep(5)   # alert send twice because of issue https://gitlab.com/signald/signald/-/issues/244
#            sendAlert()
    a=0     #reset a
    return

# Main Programm
# check GPIO input level
GPIO.add_event_detect(18, GPIO.FALLING, callback = doIfHigh, bouncetime = 500)


while 1:
    time.sleep(0.1)
    now = datetime.datetime.now()
    # noon = now.replace(hour=11, minute=59, second=0)
    # if now == noon:
    #     logging.info("noon")
    #     sendAlive()
    #     time.sleep(2)
    whole_hour = now.replace(minute=0, second=0) # writes alive to log file every hour
    timestamp = time.time()
    if now == whole_hour:
        logging.info("alive")
        time.sleep(2)
