#!/usr/bin/python3
#coding: utf8
#script to check log age every 1:10h. Paste Signal ID of contact/group instead of [ENTER SIGNAL ID HERE] on line 30

import time
import datetime
import os
import subprocess
import logging
import logging.handlers

logfile_dir = os.path.dirname(__file__)

def log_setup():
    logfilename = os.path.join(logfile_dir, '../log/pager2signal_watchdog.log')
    log_handler = logging.handlers.WatchedFileHandler(logfilename)
    formatter = logging.Formatter(
        '%(asctime)s %(levelname)s pager2signal_log_watchdog.py [%(process)d]: %(message)s',
        '%b %d %H:%M:%S')
    log_handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)

log_setup()


def send_to_old():
    global spalive
    spalive = subprocess.Popen("signaldctl message send [ENTER SIGNAL ID HERE] Logfile older than 1h10m:  Script not running", shell=True, stdout=subprocess.PIPE)
    sub_out = "\n" + spalive.stdout.read().decode('utf8')
    logging.info(sub_out)
    logging.info('Script not alive sended')


log_age = os.path.getmtime('../log/pager2signal.log')

now = time.time()

delta = now - log_age

# 1:10 hh:mm = 4200 seconds
logging.info('start watching')
if delta > 4200:
    send_to_old()
