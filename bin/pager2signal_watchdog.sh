#!/bin/bash
basedir="/home/pi/pager2signal"
logfile="${basedir}/log/pager2signal_watchdog.log"
bin="${basedir}/bin/pager2signal.py"
PID=$$

ps -ef | grep -v grep | grep pager2signal.py >/dev/null
if [ $? -ne 0 ] ; then
        echo "$(date +'%b %d %T') INFO pager2signal_watchdog.sh [${PID}]: Restarting script" >> $logfile
        python3 "${bin}" &
fi
