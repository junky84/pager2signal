A Signal based tool to get a message if a RasPi GPIO gets closed.

Primarily, the program is used to forward an alarm from an Airbus p8er such as those used by the fire department, ambulance services or other rescue organizations.

The text, that can be transmitted via Tetra is not forwarded, but instead the same previously configured text is used.

The code is designed to run on a RaspberryPi under /home/pi !
To send a message via signald the GPIO pin 18 (GPIO 24) is used.

![pager2signal image](https://gitlab.com/junky84/pager2signal/-/raw/main/pager2signal.jpg)



## Installation:
First follow the signald and signaldctl installation and configuration instructions on https://signald.org
If the setup is done and you have a running signal account clone the project code to your RasPi


```shell
cd /home/pi
git clone https://gitlab.com/junky84/pager2signal.git
```
Then set the parameters in config.ini file.
To run the tool have a look into the crontab.txt file.


## Note
The project is largely discontinued, since it is no longer actively used and developed by me. If you notice any errors in the documentation, please let me know and I will fix it.
